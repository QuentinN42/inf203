import {createServer} from "http";
import mime from "mime";
import fs from "fs";
import url from "url";

const database = JSON.parse(fs.readFileSync('./storage.json'));


function validFile(filePath) {
    if (filePath === "./")
        return false;
    if (filePath.endsWith("/"))
        return false;
    if (! fs.existsSync(filePath))
        return false;
    return true;
}

function sendFile(response, filePath) {
    const file = fs.readFileSync(filePath);
    response.end(file);
}

function setMimeType(response, filePath) {
    const mimeType = mime.getType(filePath);
    if (! mimeType)
        return console.warn(`${filePath} has no mime type`);

    response.setHeader("Content-Type", `${mimeType}; charset=utf-8`);
}

function serveFile(request, response) {
    const fileName = request.url.substring(7);
    const filePath = `./${fileName}`;
    if (! validFile(filePath))
        return sendMsg(response, `${filePath} not found.`, 404);
    setMimeType(response, filePath);
    sendFile(response, filePath);
}

function syncDb() {
    fs.writeFileSync('./storage.json', JSON.stringify(database));
}


function sendMsg(response, message, statusCode = 200) {
    response.setHeader("Content-Type", "text/html; charset=utf-8");
    response.statusCode = statusCode;
    response.end(`<!doctype html><html><body>${message}</body></html>`);
}

function get(response) {
    response.setHeader("Content-Type", "application/json; charset=utf-8");
    response.end(JSON.stringify(database));
}

function add(request, response) {
    const params = url.parse(request.url, true).query;
    const title = params.title;
    const value = parseInt(params.value);
    const color = params.color;
    console.log(`${title} : ${value} (${color})`);
    sendMsg(response, "Ok <script>window.location.href = 'Files/client/test2.html'</script>");
    database.push({
        title: title,
        value: value,
        color: color
    });
    syncDb();
}

function remove(request, response) {
    const index = url.parse(request.url, true).query.index;
    console.log(`Removing ${index}...`);
    sendMsg(response, "Ok <script>window.location.href = 'Files/client/test2.html'</script>");
    database.splice(index, 1);
    syncDb();
}

function clear(response) {
    database.length = 0;
    database.push({"title": "empty", "color": "red", "value": 1});
    sendMsg(response, "Database cleared.");
    syncDb();
}

function restore(response) {
    database.length = 0;
    JSON.parse(fs.readFileSync('./storage.json')).forEach(e => database.push(e));
    sendMsg(response, "Database restored.");
    syncDb();
}

function pchart(response) {
    const filePath = "./chart.svg";
    setMimeType(response, filePath);
    sendFile(response, filePath);
}


function webserver_wrapper(request, response) {
    // console.log(`${request.method} ${request.url} ${JSON.stringify(database)}`);
    webserver(request, response);
    // const size = request.method.length + request.url.length - 3;
    // console.log(`${response.statusCode} ${" ".repeat(size)} ${JSON.stringify(database)}`);
    console.log(`${request.method} ${request.url} ${response.statusCode}`);
}

function webserver(request, response) {
    if (request.url === '/stop')
    {
        sendMsg(response, "The server will stop now.")
        process.exit(0);
    }
    if (request.url.startsWith('/Files/'))
        return serveFile(request, response);
    if (request.url.startsWith('/Data'))
        return get(response);
    if (request.url.startsWith('/add'))
        return add(request, response);
    if (request.url.startsWith('/remove'))
        return remove(request, response);
    if (request.url.startsWith('/clear'))
        return clear(response);
    if (request.url.startsWith('/restore'))
        return restore(response);
    if (request.url === '/')
        return sendMsg(response, "Working!")
    if (request.url === '/PChart')
        return pchart(response);
    return sendMsg(response, "Not found !", 404)
}


const server = createServer(webserver_wrapper);
const port = process.argv[2] || process.env.PORT || 8000;

server.listen(port, (err) => {
    console.log(`Running on http://localhost:${port}`);
    if (err) {console.error(err);}
});
