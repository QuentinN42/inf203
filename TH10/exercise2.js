const colors = ['red', 'green', 'blue', 'yellow', 'orange', 'purple', 'pink', 'brown', 'black', 'white'];

function getText()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            const lst =this.responseText.replace('<br>', '').split('\n');
            let i = 0;
            const html = lst.reverse().filter(Boolean).map(line => `<p>${line}</p>`).join('');
            console.log(html);
            document.getElementById("tarea").innerHTML = html;
        }
    }
    xhttp.open("GET", "chatlog.txt", true);
    xhttp.send();
}

function updateText()
{
    document.getElementById("tarea").innerHTML = getText()
    setTimeout(updateText, 1000);
}

function sendText()
{
    data = document.getElementById("textedit").value;
    if (! data) return;
    
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
            console.log(this.responseText)
        }
    }
    xhttp.open("GET", `chat.php?phrase=${data}`, true);
    xhttp.send();
}

updateText()

document.getElementById("sendbut").addEventListener("click", sendText);
