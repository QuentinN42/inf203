const colors = ['red', 'green', 'blue', 'yellow', 'orange', 'purple', 'pink', 'brown', 'black', 'white'];

function loadDoc()
{
    // read the file named 'text.txt'
    // and place the content into the textarea with id tarea
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            txt = this.responseText;
            if (txt.trim().endsWith("\n"))
            {
                txt = txt.substring(0, txt.length - 1)
            }
            document.getElementById("tarea").value = txt;
        }
    }
    xhttp.open("GET", "text.txt", true);
    xhttp.send();
}

function loadDoc2()
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            const lst =this.responseText.replace('<br>', '').split('\n');
            const html = lst.filter(Boolean).map(line => `<p style='color: ${colors[lst.indexOf(line)%colors.length]}'>${line}</p>`).join('\n');
            document.getElementById("tarea2").innerHTML = html;
        }
    }
    xhttp.open("GET", "text.txt", true);
    xhttp.send();
}

// register the loadDoc funtction on click on the button with id but1
document.getElementById("but1").addEventListener("click", loadDoc);
document.getElementById("but2").addEventListener("click", loadDoc2);
