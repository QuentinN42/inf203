let x;
let pause = true;
let first = true;
let curr = -1;

fetch("slides.json").then(r => r.json()).then(_x => {x = _x.slides;}).catch(console.error);

function w(){
    console.log(`w ${curr} ${pause}`);
    setTimeout(main, 2000);
}

function main()
{
    console.log(`m ${curr} ${pause}`);
    if (! pause) {
        next();
    }
    w();
}

function play()
{
    console.log(`p ${curr} ${pause}`);
    pause = false;
    if (first)
    {
        first = false;
        main();
    }
}

function unplay()
{
    console.log(`up ${curr}`);
    pause = ! pause;
}

function display(){
    console.log(`d ${curr} ${x[curr].url}`);
    document.getElementById("SLSH").innerHTML = x[curr].url ?`<iframe src="${x[curr].url}"></iframe>`: "";
}


function wnext()
{
    console.log(`wne ${curr}`);
    pause = true;
    next();
}
function wprev()
{
    console.log(`wpr ${curr}`);
    pause = true;
    prev();
}

function next()
{
    console.log(`ne ${curr}`);
    curr = (curr + 1) % x.length;
    console.log(`ne : ${curr}`);
    display()
}

function prev()
{
    console.log(`pr ${curr}`);
    pause = true;
    curr = (curr + x.length - 1) % x.length;
    console.log(`pr : ${curr}`);
    display()
}
