let x;
let first = true;

fetch("slides.json").then(r => r.json()).then(_x => {x = _x.slides;}).catch(console.error);

function process(slide)
{
    setTimeout(() =>
    {
        document.getElementById("SLSH").innerHTML = slide.url ? `<iframe src="${slide.url}"></iframe>` : "";
    },
    slide.time * 1000);
}

function play() {
    pause = false;
    if (first)
    {
        first = false;
        x.forEach(process);
    }
}
