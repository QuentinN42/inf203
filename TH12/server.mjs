import {createServer} from "http";
import mime from "mime";
import fs from "fs";
import url from "url";


const database = [];


function validFile(filePath) {
    if (! fs.existsSync(filePath))
        return false;
    return true;
}

function sendFile(response, filePath) {
    const file = fs.readFileSync(filePath);
    response.end(file);
}

function setMimeType(response, filePath) {
    const mimeType = mime.getType(filePath);
    if (! mimeType)
        return console.warn(`${filePath} has no mime type`);

    response.setHeader("Content-Type", `${mimeType}; charset=utf-8`);
}

function serveFile(request, response) {
    const fileName = request.url.substring(7);
    const filePath = `./${fileName}`;
    if (! validFile(filePath))
        return sendMsg(response, `${filePath} not found.`, 404);
    setMimeType(response, filePath);
    sendFile(response, filePath);
}

function sendMsg(response, message, statusCode = 200) {
    response.setHeader("Content-Type", "text/html; charset=utf-8");
    response.statusCode = statusCode;
    response.end(`<!doctype html><html><body>${message}</body></html>`);
}

function bonjour(request, response) {
    const name = url.parse(request.url, true).query.visiteur;
    sendMsg(response, `bonjour ${name}`);
}

function coucou(request, response) {
    const name = url.parse(request.url, true).query.nom.replace(/<.*?>/, '');
    sendMsg(response, `coucou ${name}, the following users have already visited this page: ${database.join(', ')}`);
    database.push(name);
}

function clear(response) {
    database.length = 0;
    sendMsg(response, "Database cleared.");
}

function webserver(request, response) {
    if (request.url === '/kill')
    {
        sendMsg(response, "The server will stop now.")
        process.exit(0);
    }
    if (request.url.startsWith('/Files/'))
        return serveFile(request, response);
    if (request.url.startsWith('/bonjour'))
        return bonjour(request, response);
    if (request.url.startsWith('/coucou'))
        return coucou(request, response);
    if (request.url.startsWith('/clear'))
        return clear(response);
    sendMsg(response, "Working!")
}


const server = createServer(webserver);
const port = process.argv[2] || process.env.PORT || 8000;

server.listen(port, (err) => {
    console.log(`Running on http://localhost:${port}`);
    if (err) {console.error(err);}
});
