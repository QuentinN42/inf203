import {Stdt, FrStdt} from './exercise3.mjs';
import fs from 'fs';


export class Promo {
    // Create a module for a Promo class.
    constructor() {
        this.data = [];
    }

    // add(student) which adds a student to the promotion,
    add(student) {
        this.data.push(student);
    }

    // size() which returns the number of students in the promotion,
    size() {
        return this.data.length;
    }

    // get(i) who returns the i-th Stdt in the promotion in the order where you did add. Note: the index of the first student is 0
    get(i) {
        return this.data[i];
    }

    // print() that prints all students to the console, one per line, and returns the printed string
    print() {
        for (const s of this.data) {
            console.log(s);
        }
    }

    // write() which serializes the promotion to JSON, in other words transforms a promotion object in a string of characters,
    write() {
        return JSON.stringify(this.data);
    }

    // read(str) that reads a JSON object and rebuilds a promotion, WARNING: going through JSON.stringify then JSON.parse looses the fact that the object was a new Stdt…
    read(str) {
        this.data = [];
        let data = JSON.parse(str);
        for (const s of data) {
            if (s.nationality !== undefined) {
                this.data.push(new FrStdt(s.lastName, s.firstName, s.id, s.nationality))
            }
            else {
                this.data.push(new Stdt(s.lastName, s.firstName, s.id))
            }
        }
    }

    // saveF(fileName) that writes a promotion to a text file as a JSON object (reuse the above function write)
    saveF(fileName) {
        fs.writeFileSync(fileName, this.write());
    }    

    // readFile(fileName) which recreates a promotion from what has been saved to a file (reuse the above function read)
    readFile(fileName) {
        this.read(fs.readFileSync(fileName));
    }
}
