export function wcount(str) {
    // a function which takes a string and returns an object
    // counts the number of occurrences of this word in this string
    // The function shall return an object whose properties are the words and the values ​​of these properties are the occurrence numbers.
    // The input string is assumed to contain no punctuation and only small caps.
    let count = {};
    let words = str.split(" ");
    for (let i = 0; i < words.length; i++) {
        if (count[words[i]]) {
            count[words[i]]++;
        } else {
            count[words[i]] = 1;
        }
    }
    return count;
}


// Create a class WordL, with a constructor which takes as input a string
export class WordL {
    constructor(str) {
        this.content = wcount(str);
        this.words = [];
        for (let key in this.content) {
            this.words.push(key);
        }
    }
    
    getWords() {
        // returns an array of words present in the original text
        // lexicographically sorted and without duplicates
        let words = [...this.words];
        words.sort();
        return words;
    }

    maxCountWord() {
        // returns the word with the most occurrences
        // and if there are several words with the same number of occurences
        // returns the first of them in the lexicographically sorted list from getWords()
        let max = 0;
        let maxWord = "";
        for (let key in this.content) {
            if (this.content[key] > max) {
                max = this.content[key];
                maxWord = key;
            }
            else if (this.content[key] === max) {
                if (key < maxWord) {
                    maxWord = key;
                }
            }
        }
        return maxWord;
    }

    minCountWord() {
        // which returns the word with the least number of occurrences
        // and if there are several words with the same number of occurences
        // returns the first of them in the lexicographically sorted list from getWords()
        let min = Infinity;
        let minWord = "";
        for (let key in this.content) {
            if (this.content[key] < min) {
                min = this.content[key];
                minWord = key;
            }
            else if (this.content[key] === min) {
                if (key < minWord) {
                    minWord = key;
                }
            }
        }
        return minWord;
    }

    getCount(word) {
        // gives the number of occurrences for a given word.
        return this.content[word];
    }

    applyWordFunc(f) {
        // apply f over all words
        return this.getWords().map(f);
    }
}
