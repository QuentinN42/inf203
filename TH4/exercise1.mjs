// Question 1a:
export function fibIt(n){
    // a function which calculates the nth number of the Fibonacci sequence
    // iteratively (with a for-loop or while)
    let fib = [0, 1];
    for (let i = 2; i <= n; i++) {
        fib[i] = fib[i - 1] + fib[i - 2];
    }
    return fib[n];
}

// Question 1b:
export function fibRec(n){
    // a function which calculates the nth number of the Fibonacci sequence
    // recursively
    if (n === 0) {
        return 0;
    }
    if (n === 1) {
        return 1;
    }
    return fibRec(n - 1) + fibRec(n - 2);
}

// Question 1c:
export function fibArr(arr){
    // a function which takes an array of numbers
    // and that returns the array of results of fibRec called on the numbers
    // (without using the JS function m@p)
    let fib = [];
    for (let i = 0; i < arr.length; i++) {
        fib[i] = fibRec(arr[i]);
    }
    return fib;
}

// Question 1d:
export function fibMap(arr){
    // a function which takes an array of numbers
    // and that returns the array of results of fibRec called on the numbers
    // using the JS function map
    return arr.map(fibRec);
}
