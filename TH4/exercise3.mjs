export class Stdt{
    // A Stdt class with attributes named lastName, firstName and id
    constructor(lastName, firstName, id){
        // var student = new Stdt("Dupond", "John", 1835);
        this.lastName = lastName;
        this.firstName = firstName;
        this.id = id;
    }

    toString(){
        // "student: Dupond, John, 1835"
        return `student: ${this.lastName}, ${this.firstName}, ${this.id}`;
    }
}


export class FrStdt extends Stdt{
    // Extend the Stdt class and add nationality
    constructor(lastName, firstName, id, nationality) {
        super(lastName, firstName, id)
        this.nationality = nationality;
    }

    toString(){
        //"student: Doe, John, 432, American"
        return `${super.toString()}, ${this.nationality}`;
    }
}
