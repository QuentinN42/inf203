import fs from "fs";
import express from "express"; 

const app = express();


const database = JSON.parse(fs.readFileSync('./db.json'));

app.get('/', (req, res) => res.send('Hello World!'))
app.get('/reload', (req, res) => {database.length = 0; JSON.parse(fs.readFileSync('./db.json')).forEach(x => database.push(x)); res.type("text/plain").send('db.json reloaded')})
app.get('/countpapers', (req, res) => res.send(""+database.length))
app.get('/byauthor/:author', (req, res) => res.send(""+database.filter(x => x.authors.includes(req.params.author)).length))
app.get('/descriptors/:author', (req, res) => res.send(database.filter(x => x.authors.includes(req.params.author))))
app.get('/ttlist/:author', (req, res) => res.send(database.filter(x => x.authors.includes(req.params.author)).filter(x => Boolean(x.title)).map(x => x.title)))

const port = process.argv[2] || process.env.PORT || 8000;
app.listen(port, () => console.log(`Running on http://localhost:${port}`))
